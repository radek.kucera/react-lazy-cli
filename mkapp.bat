@echo off
setlocal enabledelayedexpansion
cls
color 03

:logo
echo      ------------------------------------------------------------------------------------------------------
echo.
echo      d8888b. d88888b  .d8b.   .o88b. d888888b   db       .d8b.  d88888D db    db    .o88b. db      d888888b
echo      88  `8D 88'     d8' `8b d8P  Y8 `~~88~~'   88      d8' `8b YP  d8' `8b  d8'   d8P  Y8 88        `88'  
echo      88oobY' 88ooooo 88ooo88 8P         88      88      88ooo88    d8'   `8bd8'    8P      88         88   
echo      88`8b   88~~~~~ 88~~~88 8b         88      88      88~~~88   d8'      88      8b      88         88   
echo      88 `88. 88.     88   88 Y8b  d8    88      88booo. 88   88  d8' db    88      Y8b  d8 88booo.   .88.  
echo      88   YD Y88888P YP   YP  `Y88P'    YP      Y88888P YP   YP d88888P    YP       `Y88P' Y88888P Y888888P
echo.     
echo      ------------------------------------------------------------------------------------------------------
echo.     

:newapp
cd "C:\\Users\Radek\Desktop\React"
echo Zde jsou vase stavajici projekty:
echo. 
@dir /b
echo.
set /p App=Zadejte jmeno aplikace: 
IF NOT EXIST %App% call npx create-react-app %App%
cd "./%App%"

:npm
cls
Echo Prave jsou instalovany npm baliky...
call npm install redux --save --loglevel silent
call npm install redux-thunk --save --loglevel silent
call npm install react-redux --save --loglevel silent
call npm install --save-dev redux-devtools --save --loglevel silent
call npm install react-router-dom --save --loglevel silent
call npm install typescript --save --loglevel silent

:file
cd "./src"
del "logo.svg"
echo. > App.css
mkdir Components

:big
call :Input
set UPSTORE=%INPUT%
call :Uppercase UPSTORE
set UPSTORE

goto reduxconf

:Input
cls
echo Vase aplikace: %App%
set INPUT=
set /P INPUT=Zadejte jmeno Reduceru: %=%
if "%INPUT%"=="" goto Input
goto:eof

:Uppercase
set %~1=!%1:a=A!
set %~1=!%1:b=B!
set %~1=!%1:c=C!
set %~1=!%1:d=D!
set %~1=!%1:e=E!
set %~1=!%1:f=F!
set %~1=!%1:g=G!
set %~1=!%1:h=H!
set %~1=!%1:i=I!
set %~1=!%1:j=J!
set %~1=!%1:k=K!
set %~1=!%1:l=L!
set %~1=!%1:m=M!
set %~1=!%1:n=N!
set %~1=!%1:o=O!
set %~1=!%1:p=P!
set %~1=!%1:q=Q!
set %~1=!%1:r=R!
set %~1=!%1:s=S!
set %~1=!%1:t=T!
set %~1=!%1:u=U!
set %~1=!%1:v=V!
set %~1=!%1:w=W!
set %~1=!%1:x=X!
set %~1=!%1:y=Y!
set %~1=!%1:z=Z!
goto:eof

:reduxconf
cls
mkdir store
cd "./store"
echo Vase aplikace: %App%
echo Jmeno reduceru: %INPUT%
set /p Store=Zadejte jmeno storu: 
set /p State=Zadejte jmeno statu: 
echo import { applyMiddleware, combineReducers, compose, createStore } from 'redux';> configureStore.js
echo import * as %Store% from "./%Store%";>> configureStore.js
echo. >> configureStore.js
echo export default function configureStore (initialState) {>> configureStore.js
echo. >> configureStore.js  
echo    const middleware = [];>> configureStore.js
echo    const enhancers = [];>> configureStore.js
echo    const rootReducer = () =^> combineReducers({>> configureStore.js
echo        %Store%: %Store%.reducer,>> configureStore.js
echo    });>> configureStore.js
echo. >> configureStore.js
echo    return createStore(>> configureStore.js
echo        rootReducer(),>> configureStore.js
echo        initialState,>> configureStore.js
echo        compose(applyMiddleware(...middleware), ...enhancers)>> configureStore.js
echo      );>> configureStore.js
echo }>> configureStore.js

:reduxstore
echo const %UPSTORE% = "%UPSTORE%";> %Store%.js
echo. >>%Store%.js
echo const initialState = { %State%: "Hello world!" };>> %Store%.js
echo. >> %Store%.js
echo export const actionCreators = {>> %Store%.js
echo     %INPUT%: (%State%) =^> {>> %Store%.js
echo         return { type: %UPSTORE%, payload: %State% };>> %Store%.js
echo     }>> %Store%.js
echo }>> %Store%.js
echo. >> %Store%.js
echo export const reducer = (state, action) =^> {>> %Store%.js
echo     state = state ^|^| initialState;>> %Store%.js
echo     switch (action.type) {>> %Store%.js
echo         case %UPSTORE%: {>> %Store%.js
echo             return { ...state, %State%: action.payload };>> %Store%.js
echo         }>> %Store%.js
echo         default: return state;>> %Store%.js
echo     }>> %Store%.js
echo }>> %Store%.js
cd "../"

:newcomp
cls
echo Vase aplikace: %App%
echo Jmeno reduceru: %INPUT%
echo Jmeno storu: %Store%
echo Jmeno statu: %State%
cd "./Components"
set /p Component=Zadejte jmeno komponenty: 
mkdir %Component%
cd "./%Component%"
echo. > %Component%.css
echo import React, { Component } from 'react';> %Component%.js
echo import './%Component%.css';>> %Component%.js
echo import { connect } from "react-redux";>>%Component%.js
echo import { actionCreators as ac%Store% } from "../../store/%Store%";>>%Component%.js
echo. >> %Component%.js
echo class %Component% extends Component {>> %Component%.js
echo. >> %Component%.js
echo   render() {>> %Component%.js
echo     return (>> %Component%.js
echo       ^<div className="%Component%"^>>> %Component%.js
echo         ^<h1^>{this.props.%State%}^</h1^>>> %Component%.js
echo       ^</div^>>> %Component%.js
echo     );>> %Component%.js
echo   }>> %Component%.js
echo }>> %Component%.js
echo. >> %Component%.js
echo export default connect(>> %Component%.js
echo   state =^> {>> %Component%.js
echo     return {>> %Component%.js
echo       %State%: state.%Store%.%State%>> %Component%.js
echo     }>> %Component%.js   
echo   },>> %Component%.js
echo   {>> %Component%.js
echo     %INPUT%: ac%Store%.%INPUT%,>> %Component%.js
echo   }>> %Component%.js
echo )(%Component%);>> %Component%.js
cd "../../"

:appcomp
del "App.js"
echo import React, { Component } from 'react';> App.js
echo import './App.css';>> App.js
echo import { Provider } from 'react-redux';>> App.js
echo //import { Switch, Router, Route } from 'react-router-dom';>> App.js
echo import %Component% from './Components/%Component%/%Component%';>> App.js
echo import configureStore from './store/configureStore';>> App.js
echo. >> App.js
echo const initialState = {};>> App.js
echo. >> App.js
echo const store = configureStore(initialState);>> App.js
echo. >> App.js
echo class App extends Component {>> App.js
echo   render() {>> App.js
echo     return (>> App.js
echo     ^<Provider store={store}^>>> App.js
echo       ^<%Component% /^>>> App.js
echo     ^</Provider^>>> App.js
echo     );>> App.js
echo   }>> App.js
echo }>> App.js
echo. >> App.js
echo export default App;>> App.js


:done
cls
echo DDDDDDDDDDDDD              OOOOOOOOO      NNNNNNNN        NNNNNNNN EEEEEEEEEEEEEEEEEEEEEE
echo D::::::::::::DDD         OO:::::::::OO    N:::::::N       N::::::N E::::::::::::::::::::E
echo D:::::::::::::::DD     OO:::::::::::::OO  N::::::::N      N::::::N E::::::::::::::::::::E
echo DDD:::::DDDDD:::::D   O:::::::OOO:::::::O N:::::::::N     N::::::N EE::::::EEEEEEEEE::::E
echo   D:::::D    D:::::D  O::::::O   O::::::O N::::::::::N    N::::::N   E:::::E       EEEEEE
echo   D:::::D     D:::::D O:::::O     O:::::O N:::::::::::N   N::::::N   E:::::E             
echo   D:::::D     D:::::D O:::::O     O:::::O N:::::::N::::N  N::::::N   E::::::EEEEEEEEEE   
echo   D:::::D     D:::::D O:::::O     O:::::O N::::::N N::::N N::::::N   E:::::::::::::::E   
echo   D:::::D     D:::::D O:::::O     O:::::O N::::::N  N::::N:::::::N   E:::::::::::::::E   
echo   D:::::D     D:::::D O:::::O     O:::::O N::::::N   N:::::::::::N   E::::::EEEEEEEEEE   
echo   D:::::D     D:::::D O:::::O     O:::::O N::::::N    N::::::::::N   E:::::E             
echo   D:::::D    D:::::D  O::::::O   O::::::O N::::::N     N:::::::::N   E:::::E       EEEEEE
echo DDD:::::DDDDD:::::D   O:::::::OOO:::::::O N::::::N      N::::::::N EE::::::EEEEEEEE:::::E
echo D:::::::::::::::DD     OO:::::::::::::OO  N::::::N       N:::::::N E::::::::::::::::::::E
echo D::::::::::::DDD         OO:::::::::OO    N::::::N        N::::::N E::::::::::::::::::::E
echo DDDDDDDDDDDDD              OOOOOOOOO      NNNNNNNN         NNNNNNN EEEEEEEEEEEEEEEEEEEEEE
echo.
echo Vase aplikace: %App%
echo Vas reducer: %INPUT%
echo Vas store: %Store%
echo Vas state: %State%
echo Vase komponenta: %Component%
echo.
echo Vasi aplikaci hostnete prikazem - npm start - a bude na adrese localhost:3000

:changecolor
set /a rand1=%random% %% 16
set /a rand2=%random% %% 16
set HEX=0123456789ABCDEF
call set hexcolors=0%%HEX:~%rand2%,1%%
color %hexcolors%
SLEEP 1
goto changecolor

endlocal