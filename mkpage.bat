@echo off

:newproject
@dir /b
set /p Project=Zadejte jmeno projektu: 
IF EXIST %Project% echo Error: Tento projekt jiz existuje! && goto newproject
mkdir %Project%
cd %Project%
mkdir src public pages
echo.> README.txt
cd "./src"
mkdir css js sass fonts

:css
cd "./css"
echo :root> main.css
echo.>> main.css
echo }>> main.css
echo.>> main.css
echo body {>> main.css
echo     font-family: 'Roboto', sans-serif;>> main.css
echo     margin: 0;>> main.css
echo     height: 100%;>> main.css
echo }>> main.css
echo. >> main.css
echo @media only screen and (max-width: 786px) {>> main.css
echo.>> main.css
echo }>> main.css

:js
cd "../js"
echo document.addEventListener("DOMContentLoaded", function(e) {> index.js
echo.>> index.js
echo });>> index.js

:public
cd "../../public"
mkdir icons
cd "../"

:indexhtml
echo ^<!DOCTYPE html^>>> index.html
echo ^<html lang="cs"^>>> index.html
echo.>>index.html
echo ^<head^>>> index.html
echo     ^<!-- Specifikování mobilních zařízení  --^>>> index.html
echo     ^<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"^>>> index.html
echo     ^<!-- Ikonka stránky --^>>> index.html
echo     ^<link rel="shortcut icon" href="./public/fav.png"^>>> index.html
echo     ^<!-- Autor --^>>> index.html
echo     ^<meta name="author" content="Radek Kučera"^>>> index.html
echo     ^<!-- Popis --^>>> index.html
echo     ^<meta name="description" content=""^>>> index.html
echo     ^<!-- Charset --^>>> index.html
echo     ^<meta charset="UTF-8"^>>> index.html
echo     ^<!-- Titulek --^>>> index.html
echo     ^<title^>%Project%^</title^>>> index.html
echo     ^<!-- GOOGLE FONTS --^>>> index.html
echo     ^<link href="https://fonts.googleapis.com/css?family=Oswald:300,500,600|Roboto:400,700" rel="stylesheet"^>>> index.html
echo     ^<!-- FONT AWESOME --^>>> index.html
echo     ^<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"^>>> index.html
echo     ^<!-- CSS --^>>> index.html
echo     ^<link rel="stylesheet" href="./src/css/main.css"^>>> index.html
echo ^</head^>>> index.html
echo.>> index.html
echo ^<body^>>> index.html
echo     ^<script src="./src/js/index.js"^>^</script^>>> index.html
echo ^</body^>>> index.html
echo.>> index.html 
echo ^</html^>>> index.html
pause